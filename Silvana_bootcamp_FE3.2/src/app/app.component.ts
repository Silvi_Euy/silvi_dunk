import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   roomList : Object[] = [
    {"id":"1", "room":"Room 1", "price":300000, "img":"/assets/img/room1.jpg", "status":"Available"},
    {"id":"2", "room":"Room 2", "price":350000, "img":"/assets/img/room2.jpg", "status":"Available"},
    {"id":"3", "room":"Room 3", "price":300000, "img":"/assets/img/room3.jpg", "status":"Not Available"},
    {"id":"4", "room":"Room 4", "price":400000, "img":"/assets/img/room4.jpg", "status":"Not Available"}
  ];
}
