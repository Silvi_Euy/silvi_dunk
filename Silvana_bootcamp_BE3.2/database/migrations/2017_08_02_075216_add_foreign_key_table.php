<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roomstatus', function (Blueprint $table){
        		$table->integer('customer_id')->unsigned();

        		$table->foreign('customer_id')->references('id')->on('customer');
        		});
        
        		$table->integer('room_id')->unsigned();

        		$table->foreign('room_id')->references('id')->on('room');
        		});

        Schema::table('transaction', function (Blueprint $table)
{
        		$table->integer('customer_id')->unsigned();

        		$table->foreign('customer_id')->references('id')->on('customer');
        		});


    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
