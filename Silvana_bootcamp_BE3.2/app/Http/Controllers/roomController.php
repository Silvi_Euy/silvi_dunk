<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\room;

class roomController extends Controller
{
    function addGuest(Request $request){
        DB::beginTransaction();
        try{

 
            $room_id = $request -> input('room_id');
            $customer_id = $request -> input('customer_id');
            $payment_status = $request -> input('payment_status');
            $check_in = $request -> input ('check_in');


            $guest = new guest;
            $guest -> room_id = $room_id;
            $guest -> customer_id = $customer_id;
            $guest -> payment_status = $payment_status;
            $guest -> check_in = $check_in;
            $guest -> save();

            

            DB::commit(); 

        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
            
        }
}
